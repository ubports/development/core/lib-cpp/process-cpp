2024-08-16 Mike Gabriel

        * Release 3.0.2 (HEAD -> main, tag: 3.0.2)

2024-03-14 Mike Gabriel

        * Merge branch 'personal/peat-psuwit/build-on-noble' into 'main'
          (7b0a829)

2024-03-04 Ratchanan Srirattanamet

        * tests: stop checking oom_score after adjusting oom{_score,}_adj
          (669ff2d)
        * debian/*: import packaging techniques from Debian where appropriate
          (6f2d0c0)
        * debian/*: upgrade to debhelper compat 13 (7dcc468)
        * CMakeLists.txt: don't compile tests if testing is disabled
          (d2dfe60)
        * CMakeLists.txt: switch C++ standard to C++17 (6e791d8)

2021-05-11 Rodney

        * Merge branch 'musl' into 'main' (ee6d99a)

2018-12-30 Luca Weiss

        * Musl libc fixes (8a9a63d)

2021-03-31 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/d-rules-parallel' into 'main'
          (119bd8e)
        * Merge branch 'personal/peat-psuwit/remove-findgtest' into 'main'
          (5847e22)

2021-03-31 Ratchanan Srirattanamet

        * d/rules: allow parallel building (3c7c8da)
        * cmake: remove FindGtest and the (commented) reference (2b39e92)

2021-03-30 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/start-building-into-repo' into
          'main' (3359a56)
        * Merge branch 'personal/peat-psuwit/fix-focal-ftbfs' into 'main'
          (0aeee18)
        * Merge branch 'personal/peat-psuwit/import-ubuntu-debian-changes'
          into 'main' (628f38e)

2021-03-31 Ratchanan Srirattanamet

        * d/control: update googletest B-Ds (61b3130)

2021-02-18 Ratchanan Srirattanamet

        * Add a Jenkinsfile, which will then trigger building into UBports
          repos (e143f84)

2021-02-17 Ratchanan Srirattanamet

        * debian/*: Bump upstream version, switch to Debian-native (8e2b13b)
        * debian/*: Adopt DEB packaging from official Debian package where
          appropriate (51e8215)

2016-06-15 Ken VanDine

        * No-change rebuild for boost soname change.;  Fix symbols file for
          s390x.;  Bump version.;  Bump major revision and so name
          to account for toolchain update. (cceb5b1)

2021-03-24 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/no-atexit' into 'master'
          (1837fde)

2021-02-18 Ratchanan Srirattanamet

        * Don't run atexit() handlers after fork() (5a56447)

2018-04-09 Marius Gripsgard

        * Fix FTBFS on bionic (2923b59) (tag: 3.0.1)
        * cleanup (5759310)
        * Imported to UBports (90915c3)

2014-07-18 CI bot

        * Releasing 2.0.0+14.10.20140718-0ubuntu1 (919dc86)

2014-07-18 thomas-voss

        * Bump major revision and so name to account for toolchain update.
          Approved by: Marcus Tomlinson, PS Jenkins bot (8ce002e)

2014-07-11 Steve Langasek

        * Drop ppc64el-specific symbol from the symbols file which no longer
          appears when building with g++4.9. (54145fa)

2014-06-27 thomas-voss

        * Bump major revision and so name to account for toolchain update.
          (7ac7aee)

2014-05-22 CI bot

        * Releasing 1.0.1+14.10.20140522-0ubuntu1 (463ec56)
        * Update symbols (dd5e22d)

2014-05-22 Pete Woods

        * Add child_setup function to exec to allow e.g. setting up AppArmor
          confinement (0cfd0e7)
        * Add missing revision from distro (4853c9b)
        * Add test case for child_setup function (9417309)

2014-05-21 Pete Woods

        * Increment the patch version (fd6f3da)
        * Dump debian version (2d0bc28)
        * Add child_setup function to exec to allow e.g. setting up AppArmor
          confinement (d99414b)

2014-03-28 CI bot

        * Releasing 1.0.0+14.04.20140328-0ubuntu1 (f672a86)

2014-03-28 Marcus Tomlinson

        * Split environment variable at first occurrence of '=' rather than
          splitting at every '=' (causes issues when a value
          contains an '=') (3ded69e)
        * Added test for env variable splitting. (a01f5d4)
        * Don't reap our child processes' children (9f94653)

2014-03-27 Marcus Tomlinson

        * Merged trunk (4643dfd)
        * Split environment variable at first occurrence of '=' rather than
          splitting at every '=' (causes issues when a value
          contains an '=') (c610e65)

2014-03-26 CI bot

        * Releasing 1.0.0+14.04.20140326.2-0ubuntu1 (78db1ab)
        * Update symbols (88f8a43)

2014-03-26 thomas-voss

        * Fix DeathObserver implementation to work correctly in
          multi-threaded cases. (8df182c)
        * Add missing symbol for ppc64el. (da654d3)

2014-03-25 thomas-voss

        * Make sure that the eventfd used by the SignalTrap is closed on
          destruction. Make sure that signal blocks are setup for
          the correct scope. (a43da41)

2014-03-24 thomas-voss

        * [ thomas-voss ] Only generate docs in release mode by default.
          (0cdfa0c)
        * Bump so name.;  Fix death observer racyness.;  Factor out
          signalfd-based wait operations on operating system
          signals.;  Introduce interface SignalTrap and accompanying
          factory functions. (29420a9)
        * Do not set but block mask. (fca74aa)
        * Scope Signal as core::posix::Signal. (d65edbc)
        * Add tests for ChildProcess::DeathObserver. Remove obsolete coud.
          (09cf4d3)
        * Make sure that we can retry death observer creation in case of
          c'tor throwing. (3a78989)
        * Adjust platform-specific symbols. (716053e)
        * Make sure that a death observer is always setup correctly.
          (8e540c5)

2014-03-23 thomas-voss

        * Factor out os signal handling into class SignalTrap. Refactor
          ChildProcess::DeathObserver and remove polling on
          signalfd. Adjust test cases. (376ca1e)

2014-03-22 CI bot

        * Releasing 0.0.1+14.04.20140322-0ubuntu1 (fa4be32)

2014-03-22 thomas-voss

        * Only generate docs in release mode by default. (99b1268)

2014-03-21 thomas-voss

        * Only generate docs in release mode by default. (72de835)

2014-03-20 thomas-voss

        * Fix DeathObserver implementation to work correctly in
          multi-threaded cases. (e49c585)

2014-03-17 CI bot

        * Releasing 0.0.1+14.04.20140317-0ubuntu1 (450cde9)

2014-03-17 Marcus Tomlinson

        * Set fd flags to *_NONBLOCK as we are checking fd.revents before
          reads anyway. Blocking reads are dangerous when recieving
          partial payloads. (6e1c67a)

2014-03-14 Marcus Tomlinson

        * Don't use signal_info if read fails. (b5329c9)
        * Set fd flags to *_NONBLOCK as we are checking fd.revents before
          reads anyway. Blocking reads are dangerous when recieving
          partial payloads. (f12a45c)

2014-03-11 CI bot

        * Releasing 0.0.1+14.04.20140311.1-0ubuntu1 (68ff562)

2014-03-11 Ricardo Salveti de Araujo

        * libprocess-cpp-dev should also depend on libproperties-cpp-dev.
          (b38547c)
        * libprocess-cpp-dev should also depend on libproperties-cpp-dev
          (14d836a)

2014-03-11 CI bot

        * Releasing 0.0.1+14.04.20140311-0ubuntu1 (c967839)
        * Update symbols (f015754)

2014-03-11 Marcus Tomlinson

        * Check that processes added to children are alive, and stop reaping
          dead processes on any error (except EINTR). (58dd81c)

2014-03-11 thomas-voss

        * Add a class DeathObserver for child processes that emits a signal
          whenever a child process goes away (either signal'd or
          exit'd). (8f6fbc1)

2014-03-10 Marcus Tomlinson

        * Fixed SIGCHLD process reaping in DeathObserver::run(). (d4e0ad3)

2014-03-07 Marcus Tomlinson

        * Some cleaning up. (283f7b3)
        * Reaping dead children (haha), should stop on any error except
          EINTR. (d9e1ef2)
        * Check that processes added to children are still alive (They may
          die between instantiation and being added) (27ec795)

2014-03-06 thomas-voss

        * Condition symbols on arch. (2d0770b)
        * Adjust symbols file. Make sure that only library symbols are
          exported. (4ff4e5d)
        * Add missing build-dependency on pkg-config. (ebae0c7)

2014-03-05 thomas-voss

        * Address review comments. (9a5dbf7)

2014-02-10 thomas-voss

        * Adjust check for error conditions to be if (syscall() == -1). Keep
          on looping if poll returns -1 and errno == EINTR. Add
          documentation pointing out the caveats when using the
          class. (5303eb6)

2014-02-04 thomas-voss

        * Add a class DeathObserver for child processes that emits a signal
          whenever a child process goes away (either signal'd or
          exit'd). (5137e40)

2014-01-25 thomas-voss

        * Print issues with the forked child to std::cerr. (8fb7c3c)

2014-01-24 thomas-voss

        * Fix copyright year. (2e73d2d)
        * Do not expose implementation details via the libraries API & ABI.
          (18b5e71)
        * Factor out backtrace generation to make it testable. (20fec60)
        * Add symbol demangling when printing the stack trace. (42b3bd7)
        * Add backtrace for std::exception case, too. (f261142)
        * Print issues with the forked child to std::cerr. (779e8af)

2014-01-22 Automatic PS uploader

        * Releasing 0.0.1+14.04.20140122-0ubuntu1 (revision 32 from
          lp:process-cpp). (efd116e)
        * Releasing 0.0.1+14.04.20140122-0ubuntu1, based on r32 (3a20e00)

2014-01-22 Łukasz 'sil2100' Zemczak

        * Add conditional symbols for other 64-bit architectures which we
          build in the archive. (e2dd92f)
        * Add conditional symbols for other 64-bit architectures which we
          build in the archive (b2c0416)

2014-01-21 Automatic PS uploader

        * Releasing 0.0.1+14.04.20140121-0ubuntu1 (revision 30 from
          lp:process-cpp). (b81a883)
        * Releasing 0.0.1+14.04.20140121-0ubuntu1, based on r30 (88aec55)
        * Update symbols (9d01c8c)

2014-01-21 Łukasz 'sil2100' Zemczak

        * Get rid of the per-architecture symbols files, make some symbols
          arch-conditional. Bump the standards version. (8285549)
        * Get rid of the per-architecture symbols files, make some symbols
          arch-conditional. Bump the standards version (82f5ae5)

2014-01-16 thomas-voss

        * Remove lcov as a build dependency. Remove unneeded cmake modules.
          Adjust and clean up cmake setup. (5307d49)

2014-01-15 thomas-voss

        * Remove lcov as a build dependency. Remove unneeded cmake modules.
          Adjust and clean up cmake setup. (8c918b6)

2014-01-09 thomas-voss

        * Added a cross-process synchronization primitive. (0508ec5)

2014-01-08 thomas-voss

        * Remove unnecessary std::cout's in core::posix::exec. (578618b)

2013-12-23 thomas-voss

        * Adjust symbol files. Adjust doxygen setup to limit dot threads to
          1, addressing armhf building issues. (f6651bd)
        * Added a cross-process synchronization primitive. (1ac6646)

2013-12-12 Automatic PS uploader

        * Releasing 0.0.1+14.04.20131212-0ubuntu1 (revision 26 from
          lp:process-cpp). (eee95c9)
        * Releasing 0.0.1+14.04.20131212-0ubuntu1, based on r26 (681c020)

2013-12-09 thomas-voss

        * Fix pkgconfig setup to account for new namespace and include
          structure. Add helper macros for cross-process testing.
          (10b75cc)

2013-12-06 thomas-voss

        * Add helper macros for cross-process testing. (6c28bc6)
        * Added more detailed documentation on the behavior of fork_and_run.
          (bdfa69f)
        * Fix pkgconfig setup to account for new namespace and include
          structure. (a1b26d1)

2013-12-03 thomas-voss

        * Refactor to namespace core. (274ed6a)

2013-12-02 thomas-voss

        * Adjust symbols files. (5121363)

2013-11-29 thomas-voss

        * Refactored directory structure to match namespace structure.
          (cd2d052)
        * First pass at namespace refactoring. Adjusted include guards and
          visibility macros. (5c9b6b9)

2013-11-18 thomas-voss

        * Introduce a testing namespace and a utility function fork_and_run
          forcross-process testing of services and clients. Refactor
          function object signatures to return a posix::exit::Status
          type instead of an int. (c648125)

2013-11-18 Timo Jyrinki

        * Add 2012, extend pkg descriptions, remove unneeded 'Section', add
          one Suggests, run wrap-and-sort -a -t. (f14e342)

2013-11-18 thomas-voss

        * Merged lp:~thomas-voss/process-cpp/add-default-value-for-env-get.
          (fd3481a)
        * Adjust throw specifiers for environment accessors. Adjust
          std::system_error to std::error_code for non-throwing
          overloads in this_process. Make c'tor of Process public to
          be able to "attach" to existing processes. (2a71b7c)

2013-11-14 thomas-voss

        * Slightly refactor the test for access to /proc/pid/stat. (1d6ea0b)
        * Removed manual debian/changelog entry. (732bca9)
        * Fix fork_and_run test cases and ensure correct results by catching
          sig_term. (e0da94a)

2013-11-12 thomas-voss

        * Adjusted symbols files to account for changed signatures and newly
          added fork_and_run method. (51a0a9e)
        * Introduce a testing namespace and a utility function fork_and_run
          for cross-process testing of services and clients.
          Refactor function object signatures to return a
          posix::exit::Status type instead of an int. (11715fe)
        * Fix doxygen comments for latex reference manual generation.
          (113eb9e)

2013-11-11 thomas-voss

        * Adjust throw specifiers for environment accessors. Adjust
          std::system_error to std::error_code for non-throwing
          overloads in this_process. Make c'tor of Process public to
          be able to "attach" to existing processes. (2c3e9f1)

2013-11-05 Timo Jyrinki

        * Add 2012, extend pkg descriptions, add one Suggests, run
          wrap-and-sort -a -t. (cdf52ab)

2013-11-05 Automatic PS uploader

        * Releasing 0.0.1+14.04.20131105.1-0ubuntu1 (revision 20 from
          lp:process-cpp). (1152510)
        * Releasing 0.0.1+14.04.20131105.1-0ubuntu1, based on r20 (fdde904)

2013-11-05 thomas-voss

        * Remove flaky test cases. (75efcca)
        * Remove flaky test cases. (0df4e49)
        * Fix failing test if test suite is run as root. (e6fa3e1)
        * Fix failing test if test suite is run as root. (c4c9a6d)

2013-11-04 thomas-voss

        *  * Refactor flags to rely on enum classes and operator overloads.
          * Add handling of process groups. (d075b70)
        * Fix symbols for i386 and armhf, accounting for long vs. int.
          (d694223)
        *  * Refactor flags to rely on enum classes and operator overloads.
          (9164962)
        * Fix symbols file. (4754183)
        * fix-test-if-executed-with-root. (d2dd5bf)
        * Add arch-specific symbol files. (17cb376)
        * Adjust symbol visibility and ensure that required symbols are
          exported. (3a27444)

2013-11-01 thomas-voss

        * Fix failing test case on buildds. (93ff611)

2013-10-26 thomas-voss

        * Adjust symbols file. (6d82c7b)
        *  * Correct indentation for tests. (1e497e9)
        *  * Make flags bitfields by defining operator| and operator&
          (5c74482)
        * Further refactoring. (46ddb8f)

2013-10-25 thomas-voss

        * Add missing graphviz build dep. (b9fe512)
        * Fix .symbols file to make bzr bd pass flawlessly again. (ad6f3c4)
        *  * Add tests checking for throw behavior and error reporting when
          querying process group ids. (432d076)

2013-10-24 thomas-voss

        * Add process group querying. (b3df6c1)

2013-10-22 thomas-voss

        *  * Add noexcept(true) specifiers where appropriate.  * Add
          documentation from man proc for
          posix::linux::proc::process::Stat. (08de603)
        * Remove posix::linux::Process, not needed right now. (eb78eca)
        * Add a README. (4641558)
        * Enhance test to check if access to this_process's streams work
          correctly. (052a34c)

2013-10-21 thomas-voss

        * Purge remaining references to environment.h/cpp (b7e60ec)
        * Refactor Self to namespace as we do not hold state. (593d5fd)
        * Add initial set of doxygen documentation. (1a0a2b8)
        *  * Add initial symbols file  * Adjust package description to be <
          80 characters. (af6f2f0)
        * Fine-tune packaging setup. (1ec2731)
        * Remove unneccessary COPYING.GPL. (7953317)
        * Add pkgconfig setup. (a153277)
        * Clean up copyright and add COPYING files. (ba6c2d1)
        * Incorporate feedback from first round of packaging review.
          (f55fcd1)
        * Add packaging setup. (d3995ca)
        * Initial push. (f6675d7)
